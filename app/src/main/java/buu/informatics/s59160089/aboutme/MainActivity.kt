package buu.informatics.s59160089.aboutme

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.view.isGone
import androidx.databinding.DataBindingUtil
import buu.informatics.s59160089.aboutme.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private val myName : MyName =  MyName("Detchaphat Soithong" , "First")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this , R.layout.activity_main)

        binding.doneButton.setOnClickListener{
            add_nick_name(it)
        }
        binding.nicknameText.setOnClickListener{
            updateNickname(it)
          }
        binding.myName = myName
    }

    fun add_nick_name (view : View) {
        val edit_text = binding.nicknameEdit//findViewById<EditText>(R.id.nickname_edit)
        val nickname_text_view = binding.nicknameText//findViewById<TextView>(R.id.nickname_text)
        myName.nickname = edit_text.text.toString()
        //nickname_text_view.text = myName.nickname
        binding.invalidateAll()

        edit_text.visibility = View.GONE
        nickname_text_view.visibility = View.VISIBLE
        //button done
        binding.doneButton.visibility = View.GONE//findViewById<Button>(R.id.done_button).visibility = View.GONE
        //hide Soft Keyboard
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken , 0)
    }

    fun updateNickname (view : View){
        val edit_text = binding.nicknameEdit//findViewById<EditText>(R.id.nickname_edit)
        val nickname_text_view = binding.nicknameText//findViewById<TextView>(R.id.nickname_text)

        edit_text.visibility = View.VISIBLE
        nickname_text_view.visibility = View.GONE
        //button done
        binding.doneButton.visibility = View.VISIBLE//findViewById<Button>(R.id.done_button).visibility = View.VISIBLE

        //focus edit text
        edit_text.requestFocus()

        //show soft keyboard
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).showSoftInput(edit_text , 0)

    }

}
